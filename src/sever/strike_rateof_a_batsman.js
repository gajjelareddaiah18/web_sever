function highest_strike_rate_players(allMatches, allDeliveries) {
    let obj = {};

    // Loop through all matches
    for (let data of allMatches) {
        // If the season is not in the object, create it
        if (!obj[data.season]) {
            obj[data.season] = {};
        }

        // Loop through all deliveries
        for (let runs of allDeliveries) {
            // If the batsman is not in the season object, create it
            if (!obj[data.season][runs.batsman]) {
                obj[data.season][runs.batsman] = { runs: 0, balls: 0 };
            }

            // Update runs and balls for the batsman in the season
            obj[data.season][runs.batsman].runs += Number(runs.batsman_runs);
            obj[data.season][runs.batsman].balls++;
        }
    }

    let strikerate = [];

    // Calculate strike rate for each batsman in each season
    for (let season in obj) {
        let maxStrikeRate = 0;
        let bestBatsman = "";

        for (let batsman in obj[season]) {
            const { runs, balls } = obj[season][batsman];
            const strikeRate = (runs / balls) * 100;

            // Check if this batsman has the highest strike rate for this season
            if (strikeRate > maxStrikeRate) {
                maxStrikeRate = strikeRate;
                bestBatsman = batsman;
            }
        }

        // Push the best batsman's data for the season to the array
        strikerate.push({ season, batsman: bestBatsman, strikeRate: maxStrikeRate });
    }

    return strikerate;
}

module.exports = highest_strike_rate_players;
